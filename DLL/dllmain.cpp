// dllmain.cpp : Defines the entry point for the DLL application.
#include "header.h"
#include <cstdio>

HHOOK original;
HMODULE _module;

LRESULT CALLBACK hook( int nCode, WPARAM wParam, LPARAM lParam ) {
	if ( nCode == HC_ACTION ) {
		if ( wParam == WM_KEYDOWN ) {
			printf( "%x\r\n", ( *( tagKBDLLHOOKSTRUCT* )lParam ).vkCode );
		}

		if ( ( *( tagKBDLLHOOKSTRUCT* )lParam ).vkCode == 0x46 ) {
			keybd_event( 0x50, 0, 0, 0 );
			keybd_event( 0x50, 0, KEYEVENTF_KEYUP, 0 );
			return 1;
		}
	}
	return CallNextHookEx( original, nCode, wParam, lParam );
}

void new_thread( ) {
	if ( AllocConsole( ) ) {
		SetConsoleTitle( "debug" );
		freopen( "CONIN$", "r", stdin );
		freopen( "CONOUT$", "w", stdout );
		freopen( "CONOUT$", "w", stderr );
	}
	MessageBox( NULL, "Hi from my dll", "Heyy!", MB_OK );
	original = SetWindowsHookExA( WH_KEYBOARD_LL, hook, 0, 0 );

	for ( ;;) {
		MSG msg;
		while ( !GetMessage( &msg, NULL, NULL, NULL ) ) {    //this while loop keeps the hook
			TranslateMessage( &msg );
			DispatchMessage( &msg );
		}
		Sleep( 100 );
	}
}


BOOL APIENTRY DllMain( HMODULE hModule,
	DWORD  ul_reason_for_call,
	LPVOID lpReserved
) {
	switch ( ul_reason_for_call ) {
	case DLL_PROCESS_ATTACH:
	{
		_module = hModule;
		CreateThread( nullptr, 0, reinterpret_cast< LPTHREAD_START_ROUTINE >( new_thread ), nullptr, 0, 0 );
		break;
	}
	case DLL_THREAD_ATTACH:
	case DLL_THREAD_DETACH:
	case DLL_PROCESS_DETACH:
		break;
	}
	return TRUE;
}

